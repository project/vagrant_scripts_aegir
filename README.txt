Aegir Vagrant Scripts
=====================

These are some nice scripts for working with Aegir and Vagrant, with a little
bit of Fabric magic thrown in.

### Requirements

1. Vagrant.
2. Fabric.

### Usage

To begin with provision a nice new Vagrant machine to play with, open a terminal
to the same directory as this file and run:

    vagrant up

Then you can run a Fabric command to do whatever you want to do:

#### Install and test the latest dev version

This will install and test the latest version of Aegir 6.x-1.x from the repos on
drupal.org.

    fab vagrant aegir_manual_install

To specific a specific version of Aegir, you can pass in a named parameter, like
so:

    fab vagrant aegir_manual_install:aegir_version=6.x-1.8

Have a look at the fabfile.py for all the possible arguments to this command.

#### Install and test the latest deb packages

This will install and test the latest version of the Aegir debs from the
unstable channel on debian.aegirproject.org.

    fab vagrant aegir_apt_install

You can choose a different channel by doing:

    fab vagrant aegir_apt_install:distro=stable

#### Clean up

When you're done testing, you will want to clean up the Vagrant machine:

    vagrant destroy

#### Debugging

If you need ssh access to the Vagrant machine from the host, just run:

    vagrant ssh
