# Really simple puppet setup manifest, just to get us started.

# We really want the latest git.
package {'git-core':
  ensure => 'latest'
}

# Need to be able to clone things with http.
package {'curl':
  ensure => 'latest'
}

# Need to be able send emails.
package {'postfix':
  ensure => 'latest'
}

# Need to install the unzip package for drush make.
package {'unzip':
  ensure => 'latest'
}

# And apache2.
package {'apache2':
  ensure => 'latest'
}

 #Install PHP
  package { ['php5',
             'php5-suhosin',
             'php5-cgi',
             'php-apc',
             'php5-cli',
             'php5-curl',
             'php5-mysql',
             'php5-gd',
             'php5-sqlite',
             'php5-xmlrpc'
  ]:
    ensure => 'latest',
    require => Class['mysql::server'],

  }

# And PHP for Apache
package {'libapache2-mod-php5':
  ensure => 'latest',
  require => Package['apache2'],
}

# And we need a DB.
class { 'mysql::server':
  status => 'running',
}


# Make sure apt is up to date before we do any package stuff.
Package {
  require => Exec["apt-update"]
}
exec { "apt-update":
  command => "/usr/bin/apt-get update"
}
